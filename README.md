# Template Kicad BE 1A Phelma

Deux répertoires de projet:
- [Projet-Kicad-S5](Projet-Kicad-S5/) pour les BE du semestre 5 (PET et PMP de novembre à janvier) avec des composants traversants
- [Projet-Kicad-S6](Projet-Kicad-S6/) pour les BE du semestre 6 (PET et PI, de janvier à mars) avec des composants montés en surface

Un répertoire commun ([Librairies](Librairies/) contenant les bibliothèques concernant les composants utilisés. On y trouve:
- les symboles (schéma électrique),
- les empreintes (PCB) et
- les modèles SPICE (simulation).
